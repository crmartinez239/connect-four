
function handleClick(event) {
    let col = event.currentTarget;
    let box = getNextBox(col);
    
    if (box != null) {
        doMove(box);
        checkVertical(col);
    }
}

function checkVertical(col) {
    const boxes = document.getElementsByClassName('box');
    let count = 0;
    let winningBoxes = [];

    for (b of boxes) {
        let who = b.getAttribute('data-selected');
        console.log(who);
    }

}

function getNextBox(col) {
    const boxes = col.getElementsByClassName('box');
    boxes.reverse();
    for (b of boxes) {
        if (b.getAttribute('data-selected') == 'none') {
            return b;
        }
    }
    return null;
}

function doMove(box) {
    drawCircle(box);
    box.setAttribute('data-selected', currentPlayer);
    if (currentPlayer == player1) {
        currentPlayer = player2;
    } else {
        currentPlayer = player1;
    }
}

function drawCircle(box) {
    let circleDiv = document.createElement('div');
    circleDiv.className = "circle";
    circleDiv.style.backgroundColor = currentPlayer;
    box.appendChild(circleDiv);
}


const player1 = 'black';
const player2 = 'red';
let currentPlayer = player1;

let columns = document.getElementsByClassName('column');
for (c of columns) {
    c.addEventListener('click', handleClick);
}